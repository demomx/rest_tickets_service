from tickets.api import *

if __name__ == '__main__':
    # default prefix from all urls tickets
    # {} - it is prefix (tickets)
    # /{}/<id_ticket>[/] - get one ticket with id_ticket method GET
    # /{}[/] - get all tickets with comments method GET
    # /{}[/] - create ticket method PUT
    # /{}/<id_ticket>/comment[/] - create comment from ticket with id_ticket method PUT
    # /{}/<id_ticket>/comments[/] - get comments from ticket with id_ticket method GET
    # /{}/<id_ticket>/status[/] - change ticket status with id_ticket 
    #                            method PUT and form body status (open,close,answer,wait)
    # /{}/<id_ticket>/status[/] - get ticket status with id_ticket method GET
    # /{}/<id_ticket>/status/[open,close,answer,wait] - change ticket status with id_ticket method PUT

    tickets = ApiTicket({
            "db":{
                "login":"",#login db
                "pass":"",#pass db
                "db":"",#db name
            }
        })
    tickets.run(host='0.0.0.0')
