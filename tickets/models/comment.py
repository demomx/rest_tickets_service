class Comment(object):
    """docstring for Comment"""
    def __init__(self, arg = {},cur = "",con = ""):
        super(Comment, self).__init__()
        self.arg = arg

        if(isinstance(cur,str) or not cur):
            raise Exception("Not found cursor")
        if(isinstance(con,str) or not con):
            raise Exception("Not found connection")
        self.db = cur
        self.connect = con
    def create(self,data = {}):
        if(isinstance(data,list)):

            columns = data.keys()
            values = [data[column] for column in columns]
            sql = "INSERT INTO comments(%s) VALUES %s RETURNING id,date_create,id_ticket;"
            try:
                self.db.execute(sql,(AsIs(','.join(columns)), tuple(values)))
                self.connect.commit();
                id_new_comment,date_creatre,id_ticket = self.db.fetchone();
                if(not id_new_comment):
                    raise Exception("Not found id")
                else:
                    data["id"] = id_new_comment
                    data["date_create"] = date_creatre
                    data["id_ticket"] = id_ticket
                    return data
            except Exception as e:
                return False
        else:
            return False