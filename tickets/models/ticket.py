from psycopg2.extensions import AsIs

class Ticket(object):
    """docstring for Ticket"""
    def __init__(self, arg = {},cur = "",con = ""):
        super(Ticket, self).__init__()
        self.arg = arg

        if(isinstance(cur,str) or not cur):
            raise Exception("Not found cursor")
        if(isinstance(con,str) or not con):
            raise Exception("Not found connection")
        self.db = cur
        self.connect = con
        self.status = ["open","close","answer","wait"] 

    def create(self,data = {}):
        if(isinstance(data,dict)):
            data['status'] = "open"
            columns = data.keys()
            values = [data[column] for column in columns]
            sql = "INSERT INTO tickets(%s) VALUES %s RETURNING id,date_creatre,date_change;"
            try:
                self.db.execute(sql,(AsIs(','.join(columns)), tuple(values)))
                self.connect.commit();
                id_new_ticket,date_creatre,date_change = self.db.fetchone();
                if(not id_new_ticket):
                    raise Exception("Not found id")
                else:
                    data["id"] = id_new_ticket
                    data["date_creatre"] = date_creatre
                    data["date_change"] = date_change
                    return data
            except Exception as e:
                return False
        else:
            return False

    def update(self,data = {}, ticket_id = 0):
        if(isinstance(data,dict)):
            columns = data.keys()
            datas = ["{}='{}'".format(column,data[column]) for column in columns]
            values = [data[column] for column in columns]
            if(len(datas)):
                sql = "UPDATE tickets SET {} WHERE id={};".format(AsIs(','.join(datas)),ticket_id)
                try:
                    self.db.execute(sql)
                    self.connect.commit();
                    data = self.get(ticket_id)
                    if(not data):
                        raise Exception("Not found id")
                    else:
                        return data
                except Exception as e:
                    print(e)
                    return False
        return False

    def comments(self,ticket_id = 0):
        if(ticket_id == 0):
            return []
        else:
            sql = """
                    SELECT * FROM comments WHERE id_ticket = %s
                    ORDER BY id DESC
                """
            self.db.execute(sql,(ticket_id))
            self.connect.commit();
            data = [dict(record) for record in self.db]
            return data

    def get(self,ticket_id = 0):
        if(ticket_id == 0):
            sql = """
                    SELECT tickets.*,array_to_json(array_agg(comments.*)) as comments 
                    FROM tickets
                    LEFT JOIN comments on comments.id_ticket = tickets.id
                    GROUP BY tickets.id
                    ORDER BY tickets.id DESC
                """
            self.db.execute(sql)
            self.connect.commit();
            data = [dict(record) for record in self.db]
        else:
            sql = """
                    SELECT tickets.*,array_to_json(array_agg(comments.*)) as comments 
                    FROM tickets
                    LEFT JOIN comments on comments.id_ticket = tickets.id
                    WHERE tickets.id = %s
                    GROUP BY tickets.id
                    ORDER BY tickets.id DESC
                """
            self.db.execute(sql,(ticket_id))
            self.connect.commit();
            data = dict(self.db.fetchone())
        return data