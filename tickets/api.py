import flask
import ujson
import psycopg2
import psycopg2.extras
import re
from flask import request
from psycopg2.extensions import AsIs
from tickets.models.ticket import *
from tickets.models.comment import *

class ApiTicket(flask.Flask):
    """docstring for ApiTicket"""
    def __init__(self, args={}):
        self.connection_config ={
            "user":"root",
            "pass":"",
            "host":"localhost",
            "port":5432,
            "dbname":"tickets",

        }
        self.prefix = "tickets";
        if(isinstance(args,dict)):
            if('db' in args):
                
                if('login' in args['db'] and
                    'pass' in args['db']):
                    self.connection_config['user'] = args['db']['login']
                    self.connection_config['pass'] = args['db']['pass']
                if('host' in args['db']):
                    self.connection_config['host'] = args['db']['host']
                if('port' in args['db']):
                    if(isinstance(args['db']['port'],int)):
                        self.connection_config['port'] = args['db']['port']
                if('db' in args['db']):
                    self.connection_config['dbname'] = args['db']['db']
                del args['db']
            if('prefix' in args):
                if(isinstance(args['prefix'],str)):
                    if(len(args['prefix'])):
                        self.prefix = args['prefix'].replace(r"^/","").replace(r"/$","")
        
        self.prefix = "/{}".format(self.prefix);

        self.connect =  psycopg2.connect(dbname=self.connection_config['dbname'], 
                                    user=self.connection_config['user'], 
                                    password=self.connection_config['pass'],
                                    host=self.connection_config['host'],
                                    port=self.connection_config['port'])
        
        self.db = self.connect.cursor(cursor_factory=psycopg2.extras.DictCursor);
        
        

        self.tickets = Ticket(cur = self.db,
                              con = self.connect)
        self.comment = Comment(cur = self.db,
                              con = self.connect)
        super(ApiTicket,self).__init__(__name__)
        self.routers=[
            {
                "routers":[ '{}','{}/<id_ticket>',
                            '{}/','{}/<id_ticket>/',],
                "fn":self.get_ticket,
                "methods":["GET"]
            },{
                "routers":['{}','{}/'],
                "fn":self.create_ticket,
                "methods":["PUT"]
            },{
                "routers":["{}/<id_ticket>/status",
                            "{}/<id_ticket>/status/"],
                "fn":self.get_ticket_status,
                "methods":["GET"]
            },{
                "routers":['{}/<id_ticket>/status/<type_status>',
                            '{}/<id_ticket>/status/<type_status>/',
                            '{}/<id_ticket>/status/',
                            '{}/<id_ticket>/status'],
                "fn":self.change_ticket_status,
                "methods":["PUT"]
            },{
                "routers":['{}/<id_ticket>/comment','{}/<id_ticket>/comment/'],
                "fn":self.create_ticket_comment,
                "methods":["PUT"]
            },{
                "routers":['{}/<id_ticket>/comments','{}/<id_ticket>/comments/'],
                "fn":self.get_ticket_comment,
                "methods":["GET"]
            }
        ]

        for router in self.routers:
            if(isinstance(router['routers'],list)):
                for path in router['routers']:
                    self.add_url_rule(path.format(self.prefix), view_func=router["fn"],methods=router["methods"])
            elif(isinstance(router['routers'],str)):
                self.add_url_rule(router['routers'].format(self.prefix), view_func=router["fn"],methods=router["methods"])


    def __return_data(self,data={},status=200,mimetype="application/json"):
        return flask.Response(response=ujson.dumps(data),status=status, mimetype=mimetype)
    
    def __validate_data(self,required_fields = [],data={}):
        errors = [];
        for field in required_fields:
            if(not field in data):
                errors.append("not found field with name %s".format(field))
            else:
                if(field == "email"):
                    if(not len(data[field])):
                        errors.append("field %s empty".format(field))
                    if(not re.match(r"[^@]+@[^@]+\.[^@]+", data[field])):
                        errors.append("email is not correct")
                elif(not len(data[field])):
                    errors.append("field %s empty".format(field))
                    data[field]=cgi.escape(data[field])
        return errors,data

    def get_ticket(self,id_ticket = 0):
        data = self.tickets.get(id_ticket)            
        return self.__return_data({
                "success":True,
                "data":data,
                "message":[]
            })

    def create_ticket(self):
        data = request.form.to_dict()
        required_fields = ['theme','text','email']
        errors,data = self.__validate_data(required_fields,data);

        if(len(errors)):
            return self.__return_data({
                    "success":False,
                    "messages":errors
                },500)

        try:
            data = self.tickets.create(data)
            if(not data):
                raise Exception('Not create ticket')
        except Exception as e:
            return self.__return_data({
                    "success":False,
                    "messages":["Fatal error!!!",e]
                },500)

        return self.__return_data({
                "success":True,
                "data":data,
                "messages":[]
            })

    def get_ticket_status(self,id_ticket = 0):
        if(not id_ticket == 0):
            data = self.tickets.get(id_ticket)    
            return self.__return_data({
                    "success":True,
                    "data":{
                        "status":data["status"]
                    }
                })
        else:
            return self.__return_data({
                    "success":False,
                    "data":{},
                    "message":"Not found ticket"
                },404)

    def change_ticket_status(self,id_ticket = 0,type_status = ""):
        data = request.form.to_dict()
        type_status = str(type_status);
        status = "";
        if(isinstance(type_status,str)):
            if(len(type_status)):
                if(type_status in self.tickets.status):
                    status = type_status
            else:
                if("status" in data):
                    data["status"] = str(data["status"])
                    if(isinstance(data["status"],str)):
                        if(data["status"] in self.tickets.status):
                            status = data["status"]
        if(len(status)):
            ticket = self.tickets.get(id_ticket);
            if(not ticket):
                return self.__return_data({
                        "success":False,
                        "data":data,
                        "message":["Not found ticket"]
                    },404)
            elif(ticket["status"]=="close"):
                return self.__return_data({
                        "success":False,
                        "data":data,
                        "message":["Ticket close"]
                    },403)
            else:
                data = self.tickets.update({"status":status},id_ticket)
                return self.__return_data({
                        "success":True,
                        "data":data
                    })
        else:
            return self.__return_data({
                    "success":False,
                    "message":["Not found status"]
                },404);
        

    def create_ticket_comment(self,id_ticket = 0):
        data = request.form.to_dict()
        required_fields = ['text','email']
        errors,data = self.__validate_data(required_fields,data);
        data_ticket = self.ticket.get(id_ticket)

        if(not data_ticket):
            errors.append('Not found ticket')
        elif(data_ticket['status'] == "close"):
            errors.append('Ticket close')

        if(len(errors)):
            return self.__return_data({
                    "success":False,
                    "messages":errors
                },(500 if(not data_ticket['status'] == "close") else 403))
        data['id_ticket'] = id_ticket
        try:
            data = self.comment.create(data)
            if(not data):
                raise Exception('Not create ticket')


        except Exception as e:
            return self.__return_data({
                    "success":False,
                    "messages":["Fatal error!!!",e]
                },500)

        return self.__return_data({
                "success":True,
                "data":data,
                "messages":[]
            })

    def get_ticket_comment(self,id_ticket = 0):
        return self.__return_data({
                "success":True,
                "data":self.tickets.comments(id_ticket)
            })